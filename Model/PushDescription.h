//
//  PushDescription.h
//  PushMeBaby
//
//  Created by iMAC on 12.08.16.
//
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    PushTypeDefault = 0,
    PushTypeVoIP = 1,
    PushTypeFetch = 2
} PushType;

@interface PushDescription : NSObject

@property(nonatomic, retain)NSString* name;
@property(nonatomic, retain)NSString* certificatePath;
@property(nonatomic, retain)NSString* certificateExtension;
@property(nonatomic, retain)NSString* payload;
@property(nonatomic, retain)NSString* deviceToken;
@property(nonatomic)PushType type;

@end
