//
//  PushDescription.m
//  PushMeBaby
//
//  Created by iMAC on 12.08.16.
//
//

#import "PushDescription.h"

@implementation PushDescription

@synthesize name = _name;
@synthesize certificatePath = _certificatePath;
@synthesize certificateExtension = _certificateExtension;
@synthesize payload = _payload;
@synthesize deviceToken = _deviceToken;
@synthesize type = _type;


-(id)copy{
    PushDescription* result = [[PushDescription alloc]init];
    result.name = self.name;
    result.certificatePath = self.certificatePath;
    result.certificateExtension = self.certificateExtension;
    result.payload = self.payload;
    result.deviceToken = self.deviceToken;
    result.type = self.type;
    return result;
}

@end
