//
//  ApplicationDelegate.m
//  PushMeBaby
//
//  Created by Stefan Hafeneger on 07.04.09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ApplicationDelegate.h"
#import "PushDescription.h"



@interface ApplicationDelegate ()
#pragma mark Properties
@property(nonatomic, retain) NSString *deviceToken, *payload, *certificate;
@property(nonatomic) PushType pushType;
@property(nonatomic, retain) NSArray *dataSource;


@property (weak) IBOutlet NSPopUpButton *pushTypePUButton;
@property (assign) IBOutlet NSButton *pushSendButton;
#pragma mark Private
- (void)connect;
- (void)disconnect;
@end

@implementation ApplicationDelegate
@synthesize pushTypePUButton = _pushTypePUButton;
@synthesize pushSendButton = _pushSendButton;

#pragma mark Allocation

- (id)init {
	self = [super init];
    self.pushTypePUButton = [[NSPopUpButton alloc] init];
	if(self != nil) {
        [self updateDataSource];
            }
	return self;
}

- (void)dealloc {
	
	// Release objects.
	self.deviceToken = nil;
	self.payload = nil;
	self.certificate = nil;
	
	// Call super.
	[super dealloc];
	
}


#pragma mark Properties

@synthesize deviceToken = _deviceToken;
@synthesize payload = _payload;
@synthesize certificate = _certificate;

#pragma mark Inherent



- (void)applicationDidFinishLaunching:(NSNotification *)notification {
    [self.pushTypePUButton removeAllItems];
    for (PushDescription* descr in self.dataSource) {
        [self.pushTypePUButton addItemWithTitle:descr.name];
    }
    [self.pushTypePUButton selectItemAtIndex:0];
    [self updateFields];

	[self connect];
}

- (void)applicationWillTerminate:(NSNotification *)notification {
	[self disconnect];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)application {
	return YES;
}

#pragma mark Private

- (void)connect {
	
    BOOL isDebug = false;
    
    if(self.certificate == nil) {
		return;
	}
	
	// Define result variable.
	OSStatus result;
	
	// Establish connection to server.
	PeerSpec peer;
    if (isDebug) {
        result = MakeServerConnection("gateway.sandbox.push.apple.com", 2195, &socket, &peer);
    } else {
        result = MakeServerConnection("gateway.push.apple.com", 2195, &socket, &peer);
    }
    NSLog(@"MakeServerConnection(): %d", result);
	
	// Create new SSL context.
	result = SSLNewContext(false, &context);// NSLog(@"SSLNewContext(): %d", result);
	
	// Set callback functions for SSL context.
	result = SSLSetIOFuncs(context, SocketRead, SocketWrite);// NSLog(@"SSLSetIOFuncs(): %d", result);
	
	// Set SSL context connection.
	result = SSLSetConnection(context, socket);// NSLog(@"SSLSetConnection(): %d", result);
	
	// Set server domain name.
    if (isDebug) {
        result = SSLSetPeerDomainName(context, "gateway.sandbox.push.apple.com", 30);
    } else {
        result = SSLSetPeerDomainName(context, "gateway.push.apple.com", 30);
    }
    //NSLog(@"SSLSetPeerDomainName(): %d", result);
	
	// Open keychain.
	result = SecKeychainCopyDefault(&keychain);// NSLog(@"SecKeychainOpen(): %d", result);
	
	// Create certificate.
	NSData *certificateData = [NSData dataWithContentsOfFile:self.certificate];
    
    certificate = SecCertificateCreateWithData(kCFAllocatorDefault, (CFDataRef)certificateData);
    if (certificate == NULL)
        NSLog (@"SecCertificateCreateWithData failled");
    
	// Create identity.
	result = SecIdentityCreateWithCertificate(keychain, certificate, &identity);// NSLog(@"SecIdentityCreateWithCertificate(): %d", result);
	
	// Set client certificate.
	CFArrayRef certificates = CFArrayCreate(NULL, (const void **)&identity, 1, NULL);
	result = SSLSetCertificate(context, certificates);// NSLog(@"SSLSetCertificate(): %d", result);
	CFRelease(certificates);
	
	// Perform SSL handshake.
	do {
		result = SSLHandshake(context);// NSLog(@"SSLHandshake(): %d", result);
	} while(result == errSSLWouldBlock);
	
}

- (void)disconnect {
	
	if(self.certificate == nil) {
		return;
	}
	
	// Define result variable.
	OSStatus result;
	
	// Close SSL session.
	result = SSLClose(context);// NSLog(@"SSLClose(): %d", result);
	
	// Release identity.
    if (identity != NULL)
        CFRelease(identity);
    identity=NULL;
	// Release certificate.
    if (certificate != NULL)
        CFRelease(certificate);
    certificate=NULL;
	// Release keychain.
    if (keychain != NULL)
        CFRelease(keychain);
    keychain=NULL;
	// Close connection to server.
	close((int)socket);
    socket=nil;
	// Delete SSL context.
	result = SSLDisposeContext(context);// NSLog(@"SSLDisposeContext(): %d", result);
    context=NULL;
}

#pragma mark Other methods
-(void)updateDataSource {
    NSMutableArray* ds = [[NSMutableArray alloc] init];
    
    PushDescription* description = [[PushDescription alloc]init];
    description.name = @"Default_indisend";
    description.certificatePath = @"indisend_push_universal";
    description.certificateExtension = @"cer";
    description.payload = @"{\"aps\":{\"alert\":\"default push\",\"sound\":\"\",\"content-available\":1}}";
    description.deviceToken = @"b063d67b c35a1e34 a9729dc9 1420da71 37860774 447a9f92 74826c7b 5e1466b7";
    //@"9b9c3bc6 ea8fad86 d1b1d4b6 af8e19aa 17ff0c30 86f997d9 5b102b19 d6624666";
    //voip token: <dc557a31 d598e3c3 9dd45960 ac8736e4 ae75ec84 e3b6e79a 28c8c211 76a6adb0>
    //Dev token: <9b9c3bc6 ea8fad86 d1b1d4b6 af8e19aa 17ff0c30 86f997d9 5b102b19 d6624666>
    description.type = PushTypeDefault;
    [ds addObject:[description copy]];
    
    description.name = @"Voip_indisend";
    description.certificatePath = @"indisend_voip_production";
    description.certificateExtension = @"cer";
    description.payload = @"{\"aps\":{\"alert\":\"voip push\",\"sound\":\"\"}}";
    description.deviceToken = @"ba5959b0 23793cb1 493b0360 0857ad3e fe7d2dc8 b4aebff8 4d108745 c9e39849";
    //@"dc557a31 d598e3c3 9dd45960 ac8736e4 ae75ec84 e3b6e79a 28c8c211 76a6adb0";
    description.type = PushTypeVoIP;
    [ds addObject:[description copy]];
    
    description.name = @"Fetch_indisend";
    description.certificatePath = @"indisend_production_1";
    description.certificateExtension = @"cer";
    description.payload = @"{\"aps\":{\"content-available\":1,\"sound\":\"\"}}";
    description.deviceToken = @"6e4e60da 89649e86 4c5fd64a 2fcce3b1 8ce6066b b02aad95 455940c2 32993c37";
    //@"9b9c3bc6 ea8fad86 d1b1d4b6 af8e19aa 17ff0c30 86f997d9 5b102b19 d6624666";
    description.type = PushTypeFetch;
    [ds addObject:[description copy]];
    
    description.name = @"VoIP_bgapp";
    description.certificatePath = @"bgapp_voip_production_25.07.2017";
    description.certificateExtension = @"cer";
    description.payload = @"{\"aps\":{\"alert\":\"voip push\",\"sound\":\"\"}}";
    description.deviceToken = @"dc557a31 d598e3c3 9dd45960 ac8736e4 ae75ec84 e3b6e79a 28c8c211 76a6adb0";
    //@"9b9c3bc6 ea8fad86 d1b1d4b6 af8e19aa 17ff0c30 86f997d9 5b102b19 d6624666";
    description.type = PushTypeVoIP;
    [ds addObject:[description copy]];
    
    description.name = @"Default_bgapp";
    description.certificatePath = @"bgapp_push_production_16.08.2017";
    description.certificateExtension = @"cer";
    description.payload = @"{\"aps\":{\"alert\":\"default push\",\"sound\":\"\"}}";
    description.deviceToken = @"9b9c3bc6 ea8fad86 d1b1d4b6 af8e19aa 17ff0c30 86f997d9 5b102b19 d6624666";
    description.type = PushTypeDefault;
    [ds addObject:[description copy]];
    
    description.name = @"Straffic";
    description.certificatePath = @"straffic_push";
    description.certificateExtension = @"cer";
    description.payload = @"{\"message\":\"privet\",\"alert\":\"privet\",\"sdInfo\":{\"sdLogin\":\"sd_test_2\",\"sdUuid\":\"19571d9f-91c7-4244-a89e-d8af31838304\"}}";
    description.deviceToken = @"0f8d9f7d 3e24c2bc fa374b8d ba12a490 d714e8f9 f6411f5d 4f3353be 71af9b89";
    description.type = PushTypeDefault;
    [ds addObject:[description copy]];
    
    description.name = @"voip_smsbank";
    description.certificatePath = @"smsbank_silent_push_17.08.2017";
    description.certificateExtension = @"cer";
    description.payload = @"{\"aps\":{\"alert\":\"voip push\",\"sound\":\"\"}}";
    description.deviceToken = @"f2c94248 9e177eb9 3330614b cd7a8481 337062cc edfdbf8c b17f39d9 0f3a62fa";
    description.type = PushTypeVoIP;
    [ds addObject:[description copy]];
    
    description.name = @"default_smsbank";
    description.certificatePath = @"smsbank_push_17.08.2017";
    description.certificateExtension = @"cer";
    description.payload = @"{\"aps\":{\"alert\":\"default push\",\"sound\":\"\",\"content-available\":1}}";
    description.deviceToken = @"2d423003 b1782601 805022bb 297eb045 ab5e6cdf 3f337a1e d4c96156 d5f686b5";
    description.type = PushTypeDefault;
    [ds addObject:[description copy]];
    
    self.dataSource = [NSArray arrayWithArray:ds];
}

-(void)updateFields{
    NSInteger selected = [self.pushTypePUButton indexOfSelectedItem];
    PushDescription* description = (PushDescription*)(self.dataSource[selected]);
    self.deviceToken = description.deviceToken;
    self.payload = description.payload;//@"{\"aps\":{\"alert\":\"voip push\",\"sound\":\"\"}}";
    self.certificate = [[NSBundle mainBundle] pathForResource:description.certificatePath ofType:description.certificateExtension];
    
}

#pragma mark IBAction

- (IBAction)pushTypeAction:(id)sender {
    [self.pushSendButton setEnabled:NO];
    [self disconnect];
    [self updateFields];
    [self connect];
    [self.pushSendButton setEnabled:YES];
    
}

- (IBAction)push:(id)sender {
	
	if(self.certificate == nil) {
        NSLog(@"you need the APNS Certificate for the app to work");
        exit(1);
	}
	
	// Validate input.
	if(self.deviceToken == nil || self.payload == nil) {
		return;
	}
	
	// Convert string into device token data.
	NSMutableData *deviceToken = [NSMutableData data];
	unsigned value;
	NSScanner *scanner = [NSScanner scannerWithString:self.deviceToken];
	while(![scanner isAtEnd]) {
		[scanner scanHexInt:&value];
		value = htonl(value);
		[deviceToken appendBytes:&value length:sizeof(value)];
	}
	
	// Create C input variables.
	char *deviceTokenBinary = (char *)[deviceToken bytes];
	char *payloadBinary = (char *)[self.payload UTF8String];
	size_t payloadLength = strlen(payloadBinary);
	
	// Define some variables.
	uint8_t command = 0;
	char message[293];
	char *pointer = message;
	uint16_t networkTokenLength = htons(32);
	uint16_t networkPayloadLength = htons(payloadLength);
	
	// Compose message.
	memcpy(pointer, &command, sizeof(uint8_t));
	pointer += sizeof(uint8_t);
	memcpy(pointer, &networkTokenLength, sizeof(uint16_t));
	pointer += sizeof(uint16_t);
	memcpy(pointer, deviceTokenBinary, 32);
	pointer += 32;
	memcpy(pointer, &networkPayloadLength, sizeof(uint16_t));
	pointer += sizeof(uint16_t);
	memcpy(pointer, payloadBinary, payloadLength);
	pointer += payloadLength;
	
	// Send message over SSL.
	size_t processed = 0;
	OSStatus result = SSLWrite(context, &message, (pointer - message), &processed);
    if (result != noErr)
        NSLog(@"SSLWrite(): %d %zd", result, processed);
	
}

@end
